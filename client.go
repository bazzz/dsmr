package dsmr

import (
	"bufio"
	"errors"
	"strconv"
	"strings"
	"time"

	"github.com/tarm/serial"
)

type Client struct {
	config       *serial.Config
	port         *serial.Port
	firstskipped bool
	OnRead       func(telegram Telegram)
}

// New returns a DSMR client prepared for listening on the given port using the given protocol version. For version 2.2 provide 2. Versions lower than 2 are not supported.
func New(portName string, protocolVersion int) (*Client, error) {
	if portName == "" {
		return nil, errors.New("portName cannot be empty")
	}
	if protocolVersion < 2 {
		return nil, errors.New("protocolVersion cannot be lower than 2")
	}
	var config serial.Config
	switch protocolVersion {
	case 2:
		config = serial.Config{Baud: 9600, Size: 7, Parity: serial.ParityEven, StopBits: 1}
		// case 4:
		// config = serial.Config{Baud: 115200, Size: 8, Parity: serial.ParityNone, StopBits: 1}
	default:
		return nil, errors.New("protocolVersion " + strconv.Itoa(protocolVersion) + " is not implemented")
	}
	config.Name = portName
	return &Client{config: &config}, nil
}

// Listen opens the serial port and calls OnRead for every received DSMR telegram. Listen is blocking.
func (c *Client) Listen() error {
	if c.config == nil {
		return errors.New("config is nil, use New() to instantiate a new client with config")
	}
	if c.OnRead == nil {
		return errors.New("handler OnRead is nil, set a handler for client.OnRead to receive DSMR telegrams")
	}
	stream, err := serial.OpenPort(c.config)
	if err != nil {
		return err
	}
	lines := make([]string, 0)
	scanner := bufio.NewScanner(stream)
	for scanner.Scan() {
		text := scanner.Text()
		lines = append(lines, text)
		if strings.HasPrefix(text, "!") {
			// The first incoming message always seems to be incomplete with my meter and cable resulting in it
			// being merged with the next one, not sure why, just skipping it and any further messages are fine.
			if !c.firstskipped {
				lines = make([]string, 0)
				c.firstskipped = true
				continue
			}
			telegram := parseTelegram(lines)
			c.OnRead(telegram)
			lines = make([]string, 0)
		}
	}
	return scanner.Err()
}

func parseTelegram(lines []string) (telegram Telegram) {
	raw := strings.Builder{}
	for i, line := range lines {
		if line == "" {
			continue
		}
		raw.WriteString(line + "\n")
		if i == 0 {
			telegram.MeterName = line
			continue
		}
		parts := strings.Split(line, "(")
		if len(parts) != 2 {
			if parts[0] == "0-1:24.3.0" {
				// special case for DSMR 2.2 Gas meter reading.
				data := strings.TrimPrefix(line, "0-1:24.3.0")
				if !strings.Contains(lines[i+1], ":") {
					data += lines[i+1]
				}
				telegram.Gas.TimeStamp, telegram.Gas.Used = parseGasReadingV2(data)
			}
			continue
		}
		obis, data := parts[0], strings.TrimSuffix(parts[1], ")")
		if data == "" {
			continue
		}
		switch obis {
		case "0-0:96.1.1":
			telegram.MeterID = data
		case "1-0:1.8.1":
			telegram.Electricity.UsedTariff1 = parseReading(data)
		case "1-0:1.8.2":
			telegram.Electricity.UsedTariff2 = parseReading(data)
		case "1-0:2.8.1":
			telegram.Electricity.DeliveredTariff1 = parseReading(data)
		case "1-0:2.8.2":
			telegram.Electricity.DeliveredTariff2 = parseReading(data)
		case "0-0:96.14.0":
			telegram.Electricity.ActiveTariff = data
		case "1-0:1.7.0":
			telegram.Electricity.CurrentUsage = parseReading(data)
		case "1-0:2.7.0":
			telegram.Electricity.CurrentDelivery = parseReading(data)
		case "0-0:96.13.1":
			telegram.TextMessageCode = data
		case "0-0:96.13.0":
			telegram.TextMessage = data
		case "0-1:96.1.0":
			telegram.Gas.ID = data
		}
	}
	telegram.Raw = raw.String()
	if telegram.TimeStamp.Year() <= 1 {
		telegram.TimeStamp = time.Now()
	}
	return
}

func parseReading(input string) (reading Reading) {
	parts := strings.Split(input, "*")
	if len(parts) != 2 {
		return
	}
	value, err := strconv.ParseFloat(parts[0], 64)
	if err != nil {
		return
	}
	reading.Value = value
	reading.Unit = parts[1]
	return
}

func parseGasReadingV2(input string) (time.Time, Reading) {
	input = strings.Trim(input, ")(")
	timestr := input[:12]
	timestamp, _ := time.ParseInLocation("060102150405", timestr, time.Local)
	gasTag := "0-1:24.2.1"
	pos := strings.Index(input, gasTag)
	reading := Reading{}
	if pos > -1 {
		pos += len(gasTag) + 1
		input = input[pos:]
		input = strings.Trim(input, ")(")
		parts := strings.Split(input, ")(")
		if len(parts) < 2 {
			return timestamp, reading
		}
		reading = parseReading(parts[1] + "*" + parts[0])
	}
	return timestamp, reading
}
