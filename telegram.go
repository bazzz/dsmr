package dsmr

import "time"

// Telegram contains the values as parsed from the smart meter message.
type Telegram struct {
	TimeStamp       time.Time
	MeterName       string
	MeterID         string
	Electricity     Electricity
	Gas             Gas
	TextMessageCode string
	TextMessage     string
	Raw             string
}

type Electricity struct {
	UsedTariff1      Reading
	UsedTariff2      Reading
	DeliveredTariff1 Reading
	DeliveredTariff2 Reading
	ActiveTariff     string
	CurrentUsage     Reading
	CurrentDelivery  Reading
}

type Gas struct {
	ID        string
	TimeStamp time.Time
	Used      Reading
}

type Reading struct {
	Value float64
	Unit  string
}
