# dsmr

Package dsmr provides a client for communication with a Dutch Smart Meter using the DSMR protocol.

## Hardware requirements

In order to use this package you would probably need a smart meter with the DSMR protocol on a P1 port and a hardware cable to connect your computer with this port in the meter.

## Protocol versions

Currently only protocol version 2.2 is implemented for the sole reason that my meter uses this version, so I could test it. However, package dsmr is made in such a way that it is fairly easy to extend the used protocols.


## Usage

Example usage:
```go
func main() {
	client, err := New("/dev/cuaU0", 2)
	if err != nil {
		log.Fatal(err)
	}
	client.OnRead = func(telegram Telegram) {
		fmt.Println(telegram)
	}
	log.Fatal(client.Listen())
}
```
